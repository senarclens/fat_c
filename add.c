#include "add.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void add(char* file_name,Fat_Head* fat_head,Root_Directory* current_root){ // add file to partition
    FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"rb+");
    if(partition_ptr == NULL){
        printf("\n%s\n",fat_head->boot_sector.partition_name);
        puts("no partition found, try again");
        return;
    }
    FILE* file_ptr = fopen(file_name,"rb+");
    if(file_ptr == NULL){
        puts("invalid file name");
        return;
    }
    unsigned int root_entry_num = 0;
    unsigned int fat_first_free_block = 0;
    unsigned int file_block_cnt = file_fat_block_count(file_name);
    for(; root_entry_num < MAX_ROOT_DIRECTORY_ENTRIES; ++root_entry_num){
        if(current_root->entries[root_entry_num].file_name[0] == '\0'){ // fill root_directory with file stats
            strcpy(current_root->entries[root_entry_num].file_name,file_name);
            current_root->entries[root_entry_num].file_size = file_size(file_name);
            current_root->entries[root_entry_num].is_dir = false;
            for(; fat_first_free_block < BLOCK_COUNT; ++fat_first_free_block){
                if(file_block_cnt == 0 && fat_head->fat.table[fat_first_free_block] == 0){
                    current_root->entries[root_entry_num].first_block = EOF;
                    break; 
                }
                if(fat_head->fat.table[fat_first_free_block] == 0){
                    current_root->entries[root_entry_num].first_block = fat_first_free_block;
                    break; 
                }
            }
            break;
        }
    }
    for(int current_block = fat_first_free_block+1, previous_block = fat_first_free_block; file_block_cnt; ++current_block){
        if((fat_head->fat.table[current_block] == 0) && (file_block_cnt > 1)){
            fat_head->fat.table[previous_block] = current_block;
            previous_block = current_block; 
            --file_block_cnt;
        }
        if((fat_head->fat.table[current_block] == 0) && (file_block_cnt == 1)){ 
            fat_head->fat.table[previous_block] = EOF; 
            previous_block = current_block; 
            --file_block_cnt;
        }
    }

    fwrite(fat_head,1,sizeof(Fat_Head),partition_ptr); // update fat_head 
    save_changes_to_current_root(fat_head,current_root); // update current_root
    void* file_one_block = malloc(BLOCK_SIZE);
    if(file_one_block == NULL){
        puts("no memory reserved for file_one_block");
        return;
    }
    for(int current_block = fat_first_free_block; current_block != EOF; current_block = fat_head->fat.table[current_block]) { // write file blocks to partition
        fread(file_one_block,1,BLOCK_SIZE,file_ptr);
        fseek(partition_ptr,sizeof(Fat_Head)+BLOCK_SIZE*current_block,SEEK_SET);
        fwrite(file_one_block,1,BLOCK_SIZE,partition_ptr);
    }
    fclose(partition_ptr);
    fclose(file_ptr);
    free(file_one_block);
    printf("written file: %s to partition: %s\n",file_name,fat_head->boot_sector.partition_name);
}

