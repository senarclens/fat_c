#ifndef MKDIR_H
#define MKDIR_H
#include "fat.h"

void mkdir(Fat_Head* fat_head,Root_Directory* current_root,char* directory_name);
void cd(Fat_Head* fat_head,Root_Directory* current_root, char* directory_name,PWD pwd[], unsigned int* pwd_size);
#endif