#include "delete.h"
#include <string.h>
#include <stdio.h>

void rm(Fat_Head* fat_head,Root_Directory* current_root,char* file_name){
 FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"rb+");
 if(partition_ptr == NULL){
   puts("no partition found, try again");
   return;
 }
 int root_entry_num = file_root_entry_num(current_root,file_name);
 unsigned int previous_block;
 for(int current_block = current_root->entries[root_entry_num].first_block; current_block != EOF;){
    previous_block = current_block;
    current_block = fat_head->fat.table[current_block];
    fat_head->fat.table[previous_block] = 0;
 }
 memset(current_root->entries[root_entry_num].file_name,'\0',32);
 save_changes_to_current_root(fat_head,current_root); // update current_root
 fwrite(fat_head,1,sizeof(Fat_Head),partition_ptr); // write fat_head to partition
 fclose(partition_ptr);
}

