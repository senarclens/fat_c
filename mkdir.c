#include "mkdir.h"
#include "stdio.h"
#include <string.h>

void mkdir(Fat_Head* fat_head,Root_Directory* current_root,char* directory_name){
    FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"rb+");
    if(partition_ptr == NULL){
        printf("\n%s\n",fat_head->boot_sector.partition_name);
        puts("no partition found, try again");
        return;
    }
    Root_Directory empty_root_directory = {0};
    for(int root_entry_number = 0; root_entry_number < MAX_ROOT_DIRECTORY_ENTRIES; ++root_entry_number){
        if(current_root->entries[root_entry_number].file_name[0] == '\0'){
            strcpy(empty_root_directory.entries[0].file_name,".");
            empty_root_directory.entries[0].file_size = BLOCK_SIZE;
            empty_root_directory.entries[0].is_dir = true;
            strcpy(empty_root_directory.entries[1].file_name,"..");
            empty_root_directory.entries[1].file_size = BLOCK_SIZE;
            empty_root_directory.entries[1].is_dir = true;
            empty_root_directory.entries[1].first_block = current_root->entries[0].first_block; // current path block is always on same position(0)
            // write directory to current root
            strcpy(current_root->entries[root_entry_number].file_name,directory_name);
            current_root->entries[root_entry_number].file_size = BLOCK_SIZE;
            current_root->entries[root_entry_number].is_dir = true;
            int fat_first_free_block = 0;
            for(;fat_first_free_block < BLOCK_COUNT; ++fat_first_free_block){
                if(fat_head->fat.table[fat_first_free_block] == 0){
                    current_root->entries[root_entry_number].first_block = fat_first_free_block;  
                    empty_root_directory.entries[0].first_block = fat_first_free_block;
                    fat_head->fat.table[fat_first_free_block] = EOF; // because one root_dir fits on one fat table block        
                    break;
                }
            }
            fwrite(fat_head,1,sizeof(Fat_Head),partition_ptr); // update fat_table 
            save_changes_to_current_root(fat_head,current_root); // update current_root
            fseek(partition_ptr,sizeof(Fat_Head)+BLOCK_SIZE*fat_first_free_block,SEEK_SET);
            fwrite(&empty_root_directory,1,sizeof(Root_Directory),partition_ptr);
            fclose(partition_ptr);
            break;
        }
    }
}

void cd(Fat_Head* fat_head,Root_Directory* current_root, char* directory_name,PWD pwd[], unsigned int* pwd_size){
    FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"rb+");
    if(partition_ptr == NULL){
        printf("\n%s\n",fat_head->boot_sector.partition_name);
        puts("no partition found, try again");
        return;
    }
    for(int root_entry_number = 0; root_entry_number < MAX_ROOT_DIRECTORY_ENTRIES; ++root_entry_number){
        if(!(strcmp(current_root->entries[root_entry_number].file_name,directory_name))){
            fseek(partition_ptr,sizeof(Fat_Head)+current_root->entries[root_entry_number].first_block*BLOCK_SIZE,SEEK_SET);
            fread(current_root,1,sizeof(Root_Directory),partition_ptr);
            if(!(strcmp(".",directory_name))){}
            else if (!(strcmp("..",directory_name))){
                if(*pwd_size > 1){ (*pwd_size)--; }
            }
            else {
                strcpy(pwd[*pwd_size].name,directory_name);
                (*pwd_size)++;
            }
            break;
        }
    }
    fclose(partition_ptr);
}


