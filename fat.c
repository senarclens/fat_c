#include "fat.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void cli(PWD pwd[], unsigned int size){
    printf("~");
    for(unsigned int i = 0; i < size; i++){
        printf("/%s",pwd[i].name);
    }
    printf("$ ");
}

void show(Fat_Head* fat_head){
    for(unsigned int i = 0; i < fat_head->boot_sector.block_count; i++){
        printf("%d %d\n",i,fat_head->fat.table[i]);
    }
}

void dir(Root_Directory* current_root){
    printf("\nfile_name                is_dir       first_block        file_size\n----------------------------------------------------------------------\n");
    for(int i = 0; current_root->entries[i].file_name[0] != '\0';i++){
        printf("%-26s %-15d %-15d %d\n",current_root->entries[i].file_name,current_root->entries[i].is_dir,
        current_root->entries[i].first_block, current_root->entries[i].file_size);
    }
    printf("\n");
}

unsigned int file_size(char* file_name){
    FILE* file_ptr = fopen(file_name, "rb");
	fseek(file_ptr, 0, SEEK_END); // set file cursor to file end
	int file_size = ftell(file_ptr);
	fclose(file_ptr);
	return file_size;   
}

unsigned int file_fat_block_count(char* file_name){
    unsigned int  block_count = file_size(file_name) / BLOCK_SIZE;
	if (file_size(file_name) % BLOCK_SIZE != 0){ ++block_count; }
	return block_count;
}

unsigned int file_root_entry_num(Root_Directory* current_root, char* file_name){
    for(int i = 0; i < MAX_ROOT_DIRECTORY_ENTRIES; i++){
        if(!(strcmp(current_root->entries[i].file_name,file_name))){
            printf("file_root_entry_num: %d\n",i);
            return i;
        }
    }
    puts("no file_root_entry_num found, try again");
    return -1;
}

int check_unique_filename(char* file_name, Root_Directory* current_root){
    for(int i = 0; i < MAX_ROOT_DIRECTORY_ENTRIES; i++){
        if(!(strcmp(file_name,current_root->entries[i].file_name))){
            return 0;            
            break;
        }
    }   
    return 1; 
}

void write_root_on_fat_table(Fat_Head* fat_head){
    FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"wb");
    if(partition_ptr == NULL){
        printf("\n%s\n",fat_head->boot_sector.partition_name);
        puts("no partition found, try again");
        return;
    }
    Root_Directory empty_root_directory = {0};
    strcpy(empty_root_directory.entries[0].file_name,".");
    empty_root_directory.entries[0].file_size = BLOCK_SIZE;
    empty_root_directory.entries[0].is_dir = true;
    empty_root_directory.entries[0].first_block = 0;
    strcpy(empty_root_directory.entries[1].file_name,"..");
    empty_root_directory.entries[1].file_size = BLOCK_SIZE;
    empty_root_directory.entries[1].is_dir = true;
    empty_root_directory.entries[1].first_block = 0;
    fat_head->fat.table[0] = EOF;
    fwrite(fat_head,1,sizeof(Fat_Head),partition_ptr); // write fat_head to partition
    fseek(partition_ptr,sizeof(Fat_Head)+BLOCK_SIZE*0,SEEK_SET);
    fwrite(&empty_root_directory,1,sizeof(Root_Directory),partition_ptr);
    fclose(partition_ptr);
}

void read_in_main_root(Fat_Head* fat_head,Root_Directory* current_root){
    FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"rb+");
    if(partition_ptr == NULL){
        printf("\n%s\n",fat_head->boot_sector.partition_name);
        puts("no partition found, try again");
        return;
    }
    fseek(partition_ptr,sizeof(Fat_Head)+BLOCK_SIZE*0,SEEK_SET); // BLOCK_SIZE*0 because maint root is on index 0 in fat table, so we need to move partition cursor at right spot
    fread(current_root,1,sizeof(Root_Directory),partition_ptr);
    fclose(partition_ptr);
}

void save_changes_to_current_root(Fat_Head* fat_head,Root_Directory* current_root){
    FILE* partition_ptr = fopen(fat_head->boot_sector.partition_name,"rb+");
    if(partition_ptr == NULL){
        printf("\n%s\n",fat_head->boot_sector.partition_name);
        puts("no partition found, try again");
        return;
    }
    fseek(partition_ptr,sizeof(Fat_Head)+current_root->entries[0].first_block*BLOCK_SIZE,SEEK_SET); // current_root->entries[0].first_block because first_block of current dir is always on same position, position 0 
    fwrite(current_root,1,sizeof(Root_Directory),partition_ptr);
    fclose(partition_ptr);
}

void man(){
    printf("\n=============================================================================\n");
    printf("\nwhen the program starts, you can create a new or use a old binary partition\npartitions should always be binary files, otherwise the program doesn't work\n\n");
    printf("rm + file_name -> remove file \n\n");
    printf("put + old_file_name  new_file_name -> write file from partition back to system\n\n");
    printf("show -> display fat table in console \n\n");
    printf("dir -> display files just like in linux ;)\n\n");
    printf("rm + file_name -> remove file \n\n");
    printf("mkdir + directory_name -> add new directory to partition\n\n");
    printf("cd + directory_name -> cd into directory\n\n");
    printf("cd .. -> go one directory back\n\n");
    printf("exit -> terminate_program\n");
    printf("\n=============================================================================\n\n");
}