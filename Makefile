CC := gcc
FLAGS := -g -Wall -Wextra -Werror -pedantic
OBJECTS := main.o fat.o partition.o add.o put.o delete.o mkdir.o
CF := main.c fat.c partition.c add.c put.c delete.c mkdir.c
APP := main

all: ${APP}

${APP}: ${OBJECTS}
	${CC} ${FLAGS} ${OBJECTS} -o $@

main.o: ${CF}
	${CC} ${FLAGS} -c ${CF} 

fat.o: fat.c fat.h
	${CC} ${FLAGS} -c fat.c 

partition.o: partition.c partition.h
	${CC} ${FLAGS} -c partition.c 

add.o: add.c add.h
	${CC} ${FLAGS} -c add.c 

put.o: put.c put.h
	${CC} ${FLAGS} -c put.c 

delete.o: delete.c delete.h
	${CC} ${FLAGS} -c delete.c 

mkdir.o: mkdir.c mkdir.h
	${CC} ${FLAGS} -c mkdir.c

.PHONY: clean
clean:
	find . -name '*~' -o -name '*.o' -o -name $(APP) | xargs rm
