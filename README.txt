Compiling the Program:
To compile the program, you must enter the command "make" in the terminal. 
This command will initiate the compilation process, and the program will be compiled.

Cleaning Object Files:
To clean object files, you must enter the command "make clean" in the terminal. 
This command will remove all the object files generated during the compilation process.

Getting More Information:
To get more information about the controls of the program, 
you must launch the program by entering the command "./main" in the terminal. 
After launching the program, you must enter "man" in the terminal, 
and it will display all the controls and instructions for using the program.


To test the program, you must follow the instructions mentioned below:

Use the image file named "img1.jpg" to test the program.
Write the image file to the binary partition by running the program.
Verify whether the image file has been successfully written to the binary partition with "dir" and "show".
If the image file has been successfully written to the binary partition, try to write the image file back to the local folder.
If the image file has been successfully written back to the local folder, the program is functioning correctly.

Author: Mihael Subasic