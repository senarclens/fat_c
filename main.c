#include "fat.h"
#include "partition.h"
#include "add.h"
#include "put.h"
#include "delete.h"
#include "mkdir.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>

int main(){
    Fat_Head fat_head = {0}; 
    Root_Directory current_root = {0};
    PWD pwd[32]; 
    unsigned int pwd_count = 0;
    char input_str[128];
    bool partition_exist = false;
    int input_partition;
    while(!partition_exist){
        char partition_name[64];
        char buffer[16];
        printf("\n1 -> to create new_partition\n2 -> to use existing partition\n3 -> to exit program\n-> ");
        fgets(buffer,16,stdin);
        sscanf(buffer,"%d",&input_partition);
        if(input_partition == 1){ 
            printf("Enter new partition name: ");
            fgets(partition_name,64,stdin);
            partition_name[strcspn(partition_name, "\n")] = '\0';
            if((partition_exist = create_new_partition(&fat_head,&current_root,partition_name))){ // create_new_partition returns bool for success
                stpcpy(pwd[pwd_count].name,partition_name);
                ++pwd_count;
            } 
        }
        else if(input_partition == 2){
            printf("Enter existing partition name: ");
            fgets(partition_name,64,stdin);
            partition_name[strcspn(partition_name, "\n")] = '\0';
            if((partition_exist = open_existing_partition(&fat_head,&current_root,partition_name))){ // open_existing_partition returns bool for success
                stpcpy(pwd[pwd_count].name,partition_name);
                ++pwd_count;
            }             
        }
        else if(input_partition == 3){
            exit(1);
        }
    }
    cli(pwd,pwd_count);
    while(fgets(input_str,128,stdin)){
        input_str[strcspn(input_str, "\n")] = '\0'; // remove new line char
        char* substr = strtok(input_str," "); // split string
        while(substr != NULL){
            if(!strcmp(substr,"add")){
                char* file_name = strtok(NULL, " "); 
                if(check_unique_filename(file_name,&current_root)) { add(file_name,&fat_head,&current_root); } 
                else{ printf("%s\n","file already exist on partition"); }
            }
            if(!(strcmp(substr,"show"))){
                show(&fat_head);
            }
            if(!(strcmp(substr,"dir"))){
                dir(&current_root);
            }
            if(!(strcmp(substr,"rm"))){
                char* file_name = strtok(NULL, " ");
                if(file_name){ rm(&fat_head,&current_root,file_name); }
            }
            if(!(strcmp(substr,"put"))){
                char* old_file_name = strtok(NULL, " ");
                char* new_file_name = strtok(NULL, " ");
                put(&fat_head,&current_root,old_file_name,new_file_name);
            }
            if(!(strcmp(substr,"mkdir"))){
                char* directory_name = strtok(NULL, " ");
                if(directory_name){ mkdir(&fat_head,&current_root,directory_name); }
            }
            if(!(strcmp(substr,"cd"))){
                char* directory_name = strtok(NULL, " ");
                if(directory_name){ cd(&fat_head,&current_root,directory_name,pwd,&pwd_count); }
            }
            if(!(strcmp(substr,"man"))){
                man();
            }
            if(!(strcmp(substr,"exit"))){
                puts("exit program");
                exit(0);
            }
            substr = strtok(NULL," "); // split till substr pointer is NULL 
        }
        cli(pwd,pwd_count);
    }
    return 0;
}







